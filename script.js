
function CassettePlayer(cassette){
    this.cassette = cassette;

    this.play = function(){
        console.log ('I am playing ${this.cassette}');
    };

    this.pause = function(){
        //pause cassette
    }

    this.stop = function (){
        //stop playing
    };

    this.record = function(){}
        //record onto a cassette
    };

this.forward = function(){
        //forward cassette
    };

this.rewind = function(){
        //rewind cassette
    };

this.eject = function(){
        //eject cassette
    };
}

let cassettePlayer = new CassettePlayer("Hip hop");
cassettePlayer.play();

console.log(cassettePlayer);
