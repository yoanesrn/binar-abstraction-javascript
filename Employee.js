class Employee{
    constructor(nama,jabatan,departement){
        this.nama = nama;
        this.jabatan = jabatan;
        this.departement = departement;
    }

    assignment(){
        return `${this.nama} mempunyai jabatan ${this.jabatan} bertugas di departemen ${this.departement}`;
    }
}

let nino = new Employee("Yoanes","CTO","Linkedin area Kulon Progo");

console.log(nino.assignment());